package com.BansalMicrosevices.UserMS.Contoller;

import com.BansalMicrosevices.UserMS.Entity.User;
import com.BansalMicrosevices.UserMS.Service.UserService;
import com.BansalMicrosevices.UserMS.VO.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }

    @GetMapping("/{id}")
    public ResponseTemplate getUserWithDepartment(@PathVariable("id") Long userId){
        return userService.getUserWithDepartment(userId);
    }



}
