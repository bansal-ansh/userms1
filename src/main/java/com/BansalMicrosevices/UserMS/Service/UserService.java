package com.BansalMicrosevices.UserMS.Service;

import com.BansalMicrosevices.UserMS.Entity.User;
import com.BansalMicrosevices.UserMS.Repository.UserRepository;
import com.BansalMicrosevices.UserMS.VO.Department;
import com.BansalMicrosevices.UserMS.VO.ResponseTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public ResponseTemplate getUserWithDepartment(Long userId) {

        ResponseTemplate vo=new ResponseTemplate();
        User user =userRepository.findByUserId(userId);

        Department department=restTemplate.getForObject("http://DEPARTMENT-SERVICE/departments/" + user.getDepartmentId(),Department.class);

        vo.setUser(user);
        vo.setDepartment(department);
        return vo;
    }
}
